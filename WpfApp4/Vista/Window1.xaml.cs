﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp4.Vista
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Window_Activated(object sender, EventArgs e)
        {

            MainWindow wnd = new MainWindow();
            wnd.Title = "Mi ventana";
            wnd.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
